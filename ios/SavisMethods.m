//
//  CustomMethods.m
//  demoRNative2
//
//  Created by VN Savis on 20/01/2024.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "AppDelegate.h"


@interface RCT_EXTERN_MODULE(SavisMethods, AppDelegate)

RCT_EXTERN_METHOD(openIosScreen: (NSString *)data completion:(RCTResponseSenderBlock)completion)

@end
