//
//  CustomMethods.swift
//  demoRNative2
//
//  Created by VN Savis on 20/01/2024.
//

import Foundation
import React
import ekyc_ios_sdk_new


@objc(SavisMethods) class SavisMethods: NSObject, UIApplicationDelegate {
  
  static func moduleName() -> String {
          return "SavisMethods"
      }

  
  @objc func openIosScreen(_ data: String, completion: @escaping RCTResponseSenderBlock) {

    
     DispatchQueue.main.async {
       NotificationCenter.default.addObserver(forName: NSNotification.Name(EKYC_SDK.shared.nameNoti), object: nil, queue: nil) { notification in
         
         var status, transactionId, code, message, callbackData: String?
         
               if let statusData = notification.userInfo?["status"] as? Int {
   
                 status = "\(statusData)"
               }
               if let transactionIdData = notification.userInfo?["transactionId"] as? String {

                 transactionId = "\(transactionIdData)"
               }
               if let codeData = notification.userInfo?["code"] as? String {

                 code = "\(codeData)"
               }
               if let messageData = notification.userInfo?["message"] as? String {

                 message = "\(messageData)"
               }
         callbackData = """
                          {"statusData" : \(status ?? ""), "transactionId" : "\(transactionId ?? "")", "code" : "\(code ?? "")", "message" : "\(message ?? "")"}
                        """

           completion([callbackData ?? ""])

       }
       
       let appDelegate = UIApplication.shared.delegate as! AppDelegate

       let data1 = data.data(using: .utf8)!

       let dataEncrypt = try! EKYC_SDK.shared.encryptUsingCBC(data: data1)
       EKYC_SDK.shared.initSDK(viewCurrent: appDelegate.uiViewCurrent, jsonString: dataEncrypt)
     }
   }
  
  deinit {
      NotificationCenter.default.removeObserver(self)
  }
                                   
}
