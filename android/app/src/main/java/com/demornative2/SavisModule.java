package com.demornative2;

import static com.example.ekycplugin.common.utils.AESUtils.cipherDecrypt;
import static com.example.ekycplugin.common.utils.AESUtils.cipherEncrypt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.ekycplugin.common.utils.CommonConstant;
import com.example.ekycplugin.presentation.EkycPluginActivity;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.io.Serializable;

public class SavisModule extends ReactContextBaseJavaModule {
    private Context mContext;
    SavisModule(ReactApplicationContext context) {
        super(context);
        this.mContext = context;
    }

    @NonNull
    @Override
    public String getName() {
        return "SavisModule";
    }

    @ReactMethod
    public void createSavisModuleEvent(String jsonStringInfo) {

        // String tmpIdentityNumber = "001098008950";
        // String tmpCredentialId = "6ab31e80f6dd8b334221aab4af50d2467221156e";
        // String tmpClientSignRequestId = "39b8afd4-75e6-4ca2-8422-b395f2b93249";
        // String tmpDeviceInfo = "{\n" +
        //         "    \"deviceId\": \"ec3d2eeaa094c8096b522810191ba2e2078a0897ad6c62bacbf4ccafef1b6348\",\n" +
        //         "    \"deviceName\": \"unknownAndroid SDK built forx86\",\n" +
        //         "    \"deviceType\": 1,\n" +
        //         "    \"ipAddress\": \"10.39.199.157\",\n" +
        //         "    \"operatingSystem\": \"Android\"\n" +
        //         "  }";

        // String info = "{\n" +
        //         "  \"baseURL\": \"https://uat.api-m.digital-id.vn/rss/1.0\",\n" +
        //         "  \"baseUrlHost2Host\": \"https://uat.api-m.digital-id.vn/token\",\n" +
        //         "  \"certProfile\": \"VPBANK_24H\",\n" +
        //         "\"credentialIds\": [\"" + tmpCredentialId + "\"],\n" +
        //         "\"clientSignRequestId\": \"" + tmpClientSignRequestId + "\"\n" + "," +
        //         "  \"consumerKey\": \"fA1ZxWHGqsr_H_gZsdwjfcFIpaoa\",\n" +
        //         "  \"deviceInforRequest\":" + tmpDeviceInfo +  ",\n" +
        //         "  \"secretKey\": \"mlEjw52RRL4Q7To22iCJfscK0Rsa\",\n" +
        //         "  \"userInfoData\": {\n" +
        //         "    \"email\": \"\",\n" +
        //         "    \"identityNumber\": \"" + tmpIdentityNumber + "\",\n" +
        //         "    \"identityType\": \"CCCD\",\n" +
        //         "    \"name\": \"\",\n" +
        //         "    \"phoneNumber\": \"\",\n" +
        //         "    \"sex\": \"\"\n" +
        //         "  }\n" +
        //         "}";

        handleBoardcast();
        Log.d("EkycPluginLog", "Input: " + jsonStringInfo);
        String info = cipherEncrypt(jsonStringInfo, "b005c2abebbdb7a846f6f643949f49d0", "b3587f493994e002");
        Intent intent = new Intent(getReactApplicationContext(), EkycPluginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bundle = new Bundle();
        bundle.putString(EkycPluginActivity.CONFIG, info);
        intent.putExtras(bundle);
        getReactApplicationContext().startActivity(intent);
    }

    void handleBoardcast() {
        final String ACTION_BOARDCAST_RECEIVER = "ACTION_BOARDCAST_RECEIVER";
        final String RESPONSE_FROM_SDK = "RESPONSE_FROM_SDK";

        IntentFilter filter = new IntentFilter(ACTION_BOARDCAST_RECEIVER);
        BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    String res = intent.getStringExtra(RESPONSE_FROM_SDK);
                    if (res != null) {
                        Log.d("EkycPluginLog", "Encrypt: " + res);
                        res = cipherDecrypt(res, CommonConstant.KEY, CommonConstant.VECTOR);
                        Log.d("EkycPluginLog", "Decrypt: " + res);
                        sendEventToReactNative(res);
                    }
                }
            }
        };
        this.mContext.registerReceiver(mReceiver, filter);
    }

    private void sendEventToReactNative(String decryptedData) {
        getReactApplicationContext()
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit("onSavisBroadcastReceived", decryptedData);
    }
}

// js
// import { DeviceEventEmitter } from 'react-native';

// // Lắng nghe sự kiện "onBroadcastReceived"
// DeviceEventEmitter.addListener('onSavisBroadcastReceived', (decryptedData) => {
//     console.log('Received decrypted data:', decryptedData);

//     // Xử lý dữ liệu ở đây
// });
