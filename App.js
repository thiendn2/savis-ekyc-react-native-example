import React from 'react';
import {
  Button,
  DeviceEventEmitter,
  NativeModules,
  Platform,
  SafeAreaView,
} from 'react-native';
// ===iOS===
// {
//   "userInfoData": {
//     "phoneNumber": "09081525088",
//     "email": "phuong@savis.vn",
//     "name": "Hoang Thanh Phuong",
//     "identityNumber": "038099006647",
//     "identityType": "CCCD",
//     "sex": 1
//   },
//   "deviceInforRequest": {
//     "deviceType": 1,
//     "deviceId": "(a)",
//     "ipAddress": "192.168.1.1",
//     "operatingSystem": "Android",
//     "deviceName": "holi"
//   },
//   "certProfile": "VPBANK_24H",
//   "credentialIds": [
//   ],
//   "clientSignRequestId": "\(UUID().uuidString.lowercased())",
//   "baseUrl": "https://uat.api-m.digital-id.vn/rss/1.0",
//   "baseUrlHost2Host": "https://uat.api-m.digital-id.vn/token",
//   "baseUrlEkycHost2Host": "https://uat.api-m.digital-id.vn/token",
//   "baseUrlEkyc" : "https://uat.api-m.digital-id.vn/uat-ekyc/v2",
//   "consumerKey": "fA1ZxWHGqsr_H_gZsdwjfcFIpaoa",
//   "secretKey": "mlEjw52RRL4Q7To22iCJfscK0Rsa",
//   "ekycConsumerKey": "YDEWv9fp6CR5gTdJN2hEc05GqIwa",
//   "ekycSecretKey": "h7IJR3gFbaPHyNUeiDgK0sU9jfAa"
// }

// ===Android===
// "{\n" +
//        "           \"userInfoData\": {\n" +
//        "             \"phoneNumber\": \"\",\n" +
//        "             \"email\": \"email@savis.vn\",\n" +
//        "             \"name\": \"Ngô Thành Chung\",\n" +
//        "             \"identityNumber\": \"\",\n" +
//        "             \"sex\": \"\",\n" +
//        "             \"identityType\": \"CCCD\"\n" +
//        "           },\n" +
//        "           \"deviceInforRequest\": {\n" +
//        "             \"deviceType\": 1,\n" +
//        "             \"deviceId\": \"TP1A.220905.001\",\n" +
//        "             \"ipAddress\": \"192.168.1.1\",\n" +
//        "             \"operatingSystem\": \"Android\",\n" +
//        "             \"deviceName\": \" deviceName \"\n" +
//        "           },\n" +
//        "           \"certProfile\": \"VPBANK_1Y\",\n" +
//        "           \"credentialIds\": [\"\"],\n" +
//        "           \"clientSignRequestId\": \"\",\n" +
//        "           \"baseURL\": \"https://uat.api-m.digital-id.vn/rss/1.0\",\n" +
// "           \"consumerKey\": \"" + BuildConfig.CONSUMER_KEY + "\",\n" +
// "           \"secretKey\": \"" + BuildConfig.CONSUMER_SECRET + "\",\n" +
// "           \"baseUrlHost2Host\": \"https://uat.api-m.digital-id.vn/token\",\n" +
// "           \"baseUrlEkyc\": \"https://uat.api-m.digital-id.vn/uat-ekyc/v2\",\n" +
// "           \"baseUrlEkycHost2Host\": \"https://uat.api-m.digital-id.vn/token\",\n" +
// "           \"ekycConsumerKey\": \"" + BuildConfig.EKYC_CONSUMER_KEY + "\",\n" +
// "           \"ekycSecretKey\": \"" + BuildConfig.EKYC_CONSUMER_SECRET + "\",\n";

const info = {
  [Platform.OS === 'android' ? 'baseURL' : 'baseUrl']:
    'https://uat.api-m.digital-id.vn/rss/3rd/1.0',
  baseUrlHost2Host: 'https://uat.api-m.digital-id.vn/token',

  baseUrlEkyc: 'https://uat.api-m.digital-id.vn/uat-ekyc/v2',
  baseUrlEkycHost2Host: 'https://uat.api-m.digital-id.vn/token',

  ekycConsumerKey: 'YDEWv9fp6CR5gTdJN2hEc05GqIwa',
  ekycSecretKey: 'h7IJR3gFbaPHyNUeiDgK0sU9jfAa',

  certProfile: 'VPBANK_24H',
  credentialIds: ['08af9dc9c47c3d582c52ab828e766f6cc30982f5'],
  clientSignRequestId: '4werwerrfhfd565gdfgf6aaaasf210',

  consumerKey: 'lSdjTeO4mGEzJdVvRaFPUc5qn1Ua',
  secretKey: 'j_83Wnv2GssC27wbMydLPgoPYPoa',

  userInfoData: {
    email: 'hungnguyen@905.vn',
    identityNumber: '030083016984',
    identityType: 'CCCD',
    name: 'Nguyễn Cao Hưng',
    phoneNumber: '0975210868',
    sex: 1, // 1 Male, 2 Female
  },
  deviceInforRequest: {
    deviceId:
      'ec3d2eeaa094c8096b522810191ba2e2078a0897ad6c62bacbf4ccafef1b6348',
    deviceName: 'unknown Android SDK built for x86',
    deviceType: 1, // Mobile = 1, Tablet = 2, Web = 3
    ipAddress: '10.39.199.157',
    operatingSystem: 'Android',
  },
};

const {SavisMethods, SavisModule} = NativeModules;

const App = () => {
  if (Platform.OS === 'android') {
    DeviceEventEmitter.addListener(
      'onSavisBroadcastReceived',
      decryptedData => {
        console.log('Received decrypted data:', decryptedData);

        // Xử lý dữ liệu ở đây
      },
    );
  }

  const onPress = () => {
    // console.log(jsonData);
    if (Platform.OS === 'android') {
      SavisModule.createSavisModuleEvent(JSON.stringify(info));
      return;
    }

    SavisMethods.openIosScreen(JSON.stringify(info), completion => {
      console.log(`Created a new event with id ${completion}`);
    });
  };

  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Button onPress={onPress} title="Open SDK" />
    </SafeAreaView>
  );
};

export default App;
